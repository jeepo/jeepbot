# We're using Alpine stable
FROM alpine:edge

#
# We have to uncomment Community repo for some packages
#
RUN sed -e 's;^#http\(.*\)/v3.9/community;http\1/v3.9/community;g' -i /etc/apk/repositories

# Installing Python 
RUN apk add --no-cache --update \
    git \
    bash \
    libffi-dev \
    openssl-dev \
    bzip2-dev \
    zlib-dev \
    readline-dev \
    sqlite-dev \
    build-base \
    python3

RUN python3 -m ensurepip \
    && pip3 install --upgrade pip setuptools \
    && rm -r /usr/lib/python*/ensurepip && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    rm -r /root/.cache


#
# Install all the required packages
#
RUN apk add --no-cache \
    py-pillow py-requests py-sqlalchemy py-psycopg2 git py-lxml \
    libxslt-dev py-pip libxml2 libxml2-dev libpq postgresql-dev \
    postgresql build-base linux-headers jpeg-dev \
    curl neofetch git sudo gcc python-dev python3-dev \
    postgresql postgresql-client php-pgsql libwebp-dev libwebp-tools \
    musl postgresql-dev chromium-chromedriver chromium \
    openjdk8-jre py3-aiohttp py-tz py3-pathlib2
RUN apk add --no-cache sqlite
RUN apk add figlet 

# Copy Python Requirements to /app
RUN git clone https://www.github.com/psycopg/psycopg2 psycopg2 \
&& cd psycopg2 \
&& python setup.py install
# Cloning Userbot Files to directory /home/
RUN mkdir /home/Telegram-UserBot
RUN git clone https://www.gitlab.com/jeepo/JeepBot /home/Telegram-UserBot
WORKDIR /home/Telegram-UserBot
# Making sudo
RUN  sed -e 's;^# \(%wheel.*NOPASSWD.*\);\1;g' -i /etc/sudoers
RUN adduser userbot --disabled-password --home /home/Telegram-UserBot
RUN adduser userbot wheel
USER userbot
#
# Install requirements
#
RUN sudo pip3 install -r requirementsDock.txt
# Removal PIP package caching
RUN rm -rf ~/.cache/pip
#
# Running userbot
#
RUN sudo chown -R userbot /home/Telegram-UserBot/userbot
RUN sudo chmod -R 777 /home/Telegram-UserBot/userbot
CMD ["python3","-m","userbot"]
