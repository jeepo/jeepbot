![jeep.img](https://gitlab.com/jeepo/jeepbot/raw/staging/src/jeep.jpg?inline=false)

# JeepBot  

JeepBot is modular telegram userbot , which run using Telethon 

made by [@jeepeo](https://t.me/jeepeo) 
# Note 
     
Most of Features are Taken from 

[Paperplane project](https://www.github.com/RaphielGang/Telegram-UserBot),

[TelegramCompanion](https://www.github.com/nitanmarcel/telegramcompanion), 

[uniborg](https://www.github.com/spechide/uniborg) 

 All features taken from them are copyrighted to their respespective, if you are handling the features/module 

 by them you need to strictly comply their respective LICENSE 


 All Exclusive Features of **JeepBot** are licensed under MIT LICENSE, This LICENSE will not cover on taken features 

 Taken Features are Covered by their respective LICENSES

