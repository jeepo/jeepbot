"""
Provides Telegram-UserBot version information.
"""

# This file is auto-generated! Do not edit!
# Use `python -m incremental.update tg_companion` to change this file.

from incremental import Version

__version__ = Version('Telegram-UserBot', 2, 4, 2)
__all__ = ["__version__"]
