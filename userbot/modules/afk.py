
# Copyright (C) 2019 The Raphielscape Company LLC.
#
# Licensed under the Raphielscape Public License, Version 1.b (the "License");
# you may not use this file except in compliance with the License.
#
""" Userbot module which contains afk-related commands """

import time

from telethon.events import StopPropagation

from userbot import (COUNT_MSG, REDIS, LOGGER, LOGGER_GROUP, USERS, HELPER, USER)
from userbot.events import register


@register(incoming=True)
async def mention_afk(mention):
    """ This function takes care of notifying the people who mention you that you are AFK."""
    global COUNT_MSG
    global USER
    AFK = REDIS.get('isafk')
    if mention.message.mentioned and not (await mention.get_sender()).bot:
        if AFK:
            if mention.sender_id not in USERS:
                print(str(AFK))
                await mention.reply(
                    f"SaD! 😐 , {USER}😎 is not here becoZ He is "
                    + AFK
                    + " . I will ping him when he comes back ! 😝 "
                )
                USERS.update({mention.sender_id: 1})
                COUNT_MSG = COUNT_MSG + 1
            elif mention.sender_id in USERS:
                if USERS[e.sender_id] % 5 == 0:
                    await mention.reply(
                        f"Again😬! {USER}😎 is not here. "
                        "I will say to him when he comes BACK!. "
                        "He told me that He is```"
                        + AFK
                        + "```"
                    )
                    USERS[mention.sender_id] = USERS[mention.sender_id] + 1
                    COUNT_MSG = COUNT_MSG + 1
                else:
                    USERS[mention.sender_id] = USERS[mention.sender_id] + 1
                    COUNT_MSG = COUNT_MSG + 1


@register(incoming=True)
async def afk_on_pm(sender):
    global USERS
    global COUNT_MSG
    AFK = REDIS.get('isafk')
    if sender.is_private and not (await sender.get_sender()).bot:
        if AFK:
            if sender.sender_id not in USERS:
                await sender.reply(
                    f"SaD! 😐 , {USER}😎 is not here becoZ He is ```"
                    + AFK
                    + "```I will ping him when he comes back ! 😝 "
                )
                USERS.update({sender.sender_id: 1})
                COUNT_MSG = COUNT_MSG + 1
            elif sender.sender_id in USERS:
                if USERS[sender.sender_id] % 5 == 0:
                    await sender.reply(
                        "Again😬! {USER}😎 is not here. "
                        "I will say to him when he comes BACK!. "
                        "He told me that He is```"
                        + AFK
                        + "``"
                    )
                    USERS[sender.sender_id] = USERS[sender.sender_id] + 1
                    COUNT_MSG = COUNT_MSG + 1
                else:
                    USERS[sender.sender_id] = USERS[sender.sender_id] + 1
                    COUNT_MSG = COUNT_MSG + 1


@register(outgoing=True, pattern="^.afk")
async def set_afk(sender):
    if not sender.text[0].isalpha() and sender.text[0] not in ("/", "#", "@", "!"):
        message = sender.text
        try:
            AFKREASON = str(message[5:])
        except:
            AFKREASON = ''
        if not AFKREASON:
            AFKREASON = 'Not online'
        await sender.edit("AFK AF!")
        if LOGGER:
            await sender.client.send_message(LOGGER_GROUP, "You went AFK!")
        REDIS.set('isafk', AFKREASON)
        AFK = REDIS.get('isafk')
        print(str(AFK))
        raise StopPropagation


@register(outgoing=True)
async def type_afk_is_not_true(notafk):
    global COUNT_MSG
    global USERS
    global AFKREASON
    ISAFK = REDIS.get('isafk')
    if ISAFK:
        REDIS.delete('isafk')
        await notafk.respond("I'm no longer AFK.")
        x = await notafk.respond(
            "`You recieved "
            + str(COUNT_MSG)
            + " messages while you were away. Check log for more details.`"
            + "`This auto-generated message shall be self destructed in 2 seconds.`"
        )
        time.sleep(2)
        await notafk.delete()
        if LOGGER:
            await notafk.client.send_message(
                LOGGER_GROUP,
                "You've recieved "
                + str(COUNT_MSG)
                + " messages from "
                + str(len(USERS))
                + " chats while you were away",
            )
            for i in USERS:
                name = await notafk.client.get_entity(i)
                name0 = str(name.first_name)
                await notafk.client.send_message(
                    LOGGER_GROUP,
                    "["
                    + name0
                    + "](tg://user?id="
                    + str(i)
                    + ")"
                    + " sent you "
                    + "`"
                    + str(USERS[i])
                    + " messages`",
                )
        COUNT_MSG = 0
        USERS = {}
        AFKREASON = "Not Online"

HELPER.update({
    "afk": ".afk <reason>(reason is optional)\
\nUsage: Sets you as afk. Responds to anyone who tags/PM's \
you telling that you are afk. Switches off AFK when you type back anything.\
"
})
