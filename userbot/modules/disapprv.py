#
# Copyright (c) Yasir siddique | 2019
#
from telethon import TelegramClient, events
from telethon.tl.functions.contacts import BlockRequest

from userbot import COUNT_PM, LOGGER, LOGGER_GROUP, NOTIF_OFF, PM_AUTO_BAN, USER, bot


@bot.on(events.NewMessage(outgoing=True, pattern="^.disapprove$"))
@bot.on(events.MessageEdited(outgoing=True, pattern="^.disapprove$"))
async def disapprovepm(e):
    if not e.text[0].isalpha() and e.text[0] not in ("/", "#", "@", "!"):
        try:
            from userbot import MONGO
        except:
            await e.edit("`Running on Non-SQL mode!`")
            return
        if e.reply_to_msg_id:
            reply = await apprvpm.get_reply_message()
            replied_user = await apprvpm.client(GetFullUserRequest(reply.from_id))
            name0 = str(replied_user.user.first_name)
            uid = replied_user.user.id
        else:
            env = await bot.get_entity(e.chat_id)
            name0 = str(env.first_name)
            uid = e.chat_id

    disappv = MONGO.pmpermit.find_one(
               {"chat_id":uid}
                )
    if not disappv:
        await e.edit(f"{name0} isn't approved to disapprove😐")
    else:
        MONGO.pmpermit.delete_one(
            {"chat_id":uid}
        )
        await e.edit(f"`Sad!😐 {name0} has been disapproved to PM {USER}😎`")
    if LOGGER:
        aname = await bot.get_entity(e.chat_id)
        name0 = str(aname.first_name)
        await bot.send_message(
            LOGGER_GROUP,
            "#DISAPPROVE \n\n"
            +"["
            + name0
            + "](tg://user?id="
            + str(uid)
            + ")"
            + f" Was disapproved to PM! {USER}😎.",
        )
